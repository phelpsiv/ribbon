# Chat App
A blatant clone of [Franz](https://meetfranz.com/).
We were promted to build this as a learning exercise for bettering our React,
Redux, and Electron skills, and I (@phelpsiv) hate Franz's logo. That said,
other than the logo, we love Franz and think you should support them as we're
likely not going to be maintaining this anywhere near as well as they'll be
maintaining their app.

## Package Dependencies
Note: I'll add versioning later

- [Electron](electronjs.org)
- [React](reactjs.org)
- [Redux](redux.js.org)
- [Redux-Saga](redux-saga.js.org)

## Dev Dependencies

- [Babel](babeljs.io)
- Lots of bable plugins
- [Webpack](webpack.js.org)
- Lots of webpack plugins
- [ESLint](eslint.org)
- [Jest](jestjs.io)
- [Enzyme](airbnb.io/enzyme/)