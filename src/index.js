import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import './index.css';
import App from './App/AppComponent';
import store, { sagaMiddleware } from './store';
import rootSaga from './sagas.js'
//import registerServiceWorker from './registerServiceWorker';

sagaMiddleware.run(rootSaga);

ReactDOM.render(
  <Provider store={ store }>
    <App />
  </Provider>,
  document.getElementById('root')
);
//registerServiceWorker();
