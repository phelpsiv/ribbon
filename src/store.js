import { createStore, applyMiddleware, compose } from 'redux';
import 'regenerator-runtime/runtime';
import createSagaMiddleware from 'redux-saga';
import rootReducer from './reducers';

export const sagaMiddleware = createSagaMiddleware();

const savedState = typeof window.localStorage !== undefined && window.localStorage.getItem('state') ? JSON.parse(window.localStorage.getItem('state')) : {};

const middleware = [
  sagaMiddleware
];
const enhancers = [];

if (process.env.NODE_ENV === 'development') {
  const devToolsExtension = window.__REDUX_DEVTOOLS_EXTENSION__;

  if (typeof devToolsExtension === 'function') {
    enhancers.push(devToolsExtension());
  }
}

const composedEnhancers = compose(
  applyMiddleware(...middleware),
  ...enhancers
);

const store = createStore(
  rootReducer,
  savedState,
  composedEnhancers
);

if (typeof window.localStorage !== undefined) {
  store.subscribe(()=>{
    window.localStorage.setItem('state', JSON.stringify({ auth: store.getState() }));
  });
}

export default store;
